# Alkalmazott szilárdtestfizika

Ez a jegyzet a BME TTK Fizika BSc hallgatóinak készült az "Alkalmazott szilárdtestfizika" tantárgyhoz. A jegyzetet a vizsgára való felkészülés közben írtam.


## Linkek

* ["Alkalmazott szilárdtestfizika (BMETE11AF11)" tantárgy honlapja](http://fizipedia.bme.hu/index.php/Alkalmazott_szil%C3%A1rdtestfizika)
* [BME-ELTE Nanofizika tudásbázis](http://fizipedia.bme.hu/index.php/Nanofizika_tud%C3%A1sb%C3%A1zis)


## LaTeX ⇢ PDF

### [`blang/latex-docker`](https://github.com/blang/latex-docker)

Telepítsd fel a Dockert, majd futtasd a következő parancsokat

```
$ docker pull blang/latex
$ chmod +x docker-latex.sh
```

Ezután a következő paranccsal fordíthatod le például a `main.tex` fájlt.

```
$ ./docker-latex.sh main.tex
```



## TODO:

* Is there anything like GitHub Releases on GitLab? Could I make creating the PDF file from source part of the build process (GitLab CI with a docker image that has all the LaTeX command line magic installed)? If there isn't, look into githooks. There must be some sane solution to this
* Include this README in the LaTeX+PDF file, so I don't need to duplicate text
* Add git hash and timestamp/date to the generated PDF, so readers will know whether it's the latest version

> [Project Avatar](project-avatar.jpg) edited with [Croppola](http://croppola.com/) // [CC0](https://www.pexels.com/photo/black-and-blue-electronic-tools-on-green-circuit-board-39290/)
